import React, { useState, useRef } from 'react';
import { SafeAreaView, ScrollView, StatusBar, StyleSheet, TextInput } from 'react-native';
import { event, onChange } from 'react-native-reanimated';
import { Path, Text } from 'react-native-svg';
import { Box } from './layout/box'
const styles = StyleSheet.create({
    digit: {
        width: 40,
        height: 47,
        backgroundColor: '#F2F2F7',
        borderRadius: 4,
        textAlign: 'center',

    }
})
type data = {
    setCode: Function,
    code: any
}
export const DigitInput: React.FC<data> = ({ setCode, code }): any => {
    let refs = [
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
    ]
    const inputRefManage = (event: any, id: number) => {
        if (1 < id && event.nativeEvent.key == 'Backspace') {
            refs[id - 2].current?.focus()
        } else if (event.nativeEvent.key !== 'Backspace' && id < 6) {
            refs[id].current?.focus()
        }
    }
    return (
        <Box height={56} flexDirection='row' justifyContent='space-evenly' alignItems='center'>
            <TextInput ref={refs[0]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setCode({ ...code, pin1: pin }) }} onKeyPress={event => { inputRefManage(event, 1) }} />
            <TextInput ref={refs[1]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setCode({ ...code, pin2: pin }) }} onKeyPress={event => { inputRefManage(event, 2) }} />
            <TextInput ref={refs[2]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setCode({ ...code, pin3: pin }) }} onKeyPress={event => { inputRefManage(event, 3) }} />
            <TextInput ref={refs[3]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setCode({ ...code, pin4: pin }) }} onKeyPress={event => { inputRefManage(event, 4) }} />
            <TextInput ref={refs[4]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setCode({ ...code, pin5: pin }) }} onKeyPress={event => { inputRefManage(event, 5) }} />
            <TextInput ref={refs[5]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setCode({ ...code, pin6: pin }) }} onKeyPress={event => inputRefManage(event, 6)} />
        </Box>
    );
};
