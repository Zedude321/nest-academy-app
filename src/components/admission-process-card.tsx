import React from 'react'
import { Border, Shadow } from './core'
import { Spacing, Box, Stack } from './layout'

export const AdmissionProcessCardHeader: React.FC<any> = ({ children }) => {
    return (
        <Spacing pb={3}>
            <Box flexDirection={'column'} height={'auto'}>
                <Spacing pl={3} pb={3}>
                    <Stack size={2}>
                        {children}
                    </Stack>
                </Spacing>
                <Box width={'100%'} height={1} role={'primary200'}></Box>
            </Box>
        </Spacing>
    ) 
}

export const AdmissionProcessCardContent: React.FC<any> = ({ children }) => {

    return (
        <Spacing pl={3} pr={3}>
            <Box flexDirection={'column'} height={'auto'}>
                <Stack size={2}>
                    {children}
                </Stack>
            </Box>
        </Spacing>
    )
}

export const AdmissionProcessCard: React.FC<any> = ({ children }) => {

    return (

        <Shadow h={2} w={0} radius={4} opacity={0.25} role={'primary500'}>
            <Shadow radius={2} h={0} w={0} opacity={0.25} role={'primary500'}>
                <Border radius={4}>
                    <Box width={'100%'} height={'auto'} role={'white'}>


                        <Spacing pt={3} pb={3}>
                            <Box width={'100%'} height={'auto'}>
                                {children}
                            </Box>
                        </Spacing>

                    </Box>
                </Border>

            </Shadow>
        </Shadow>
    )
}