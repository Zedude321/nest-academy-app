import React, { useEffect, useRef, FC } from 'react';
import { Animated } from 'react-native';
import { useTheme } from '../theme-provider';
import { ColorType } from '../types';

export const AnimatedDynamicView: FC<DynamicViewType> = ({
    position,
    visible,
    children,
    role,
    width,
    height,
    duration = 50,
}) => {
    const { colors } = useTheme();
    const heightAnim = useRef(new Animated.Value(0)).current;
    const zIndex = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        Animated.parallel([
            Animated.timing(heightAnim, {
                duration,
                useNativeDriver: false,
                toValue: visible ? height : 0,
            }),
            Animated.timing(zIndex, {
                duration,
                useNativeDriver: false,
                toValue: visible ? 1 : -100,
            }),
        ]).start();
    }, [visible]);

    return (
        <Animated.View
            style={{
                maxHeight: heightAnim,
                width,
                position,
                backgroundColor: role ? colors[role] : 'transparent',
                zIndex,
            }}
        >
            {children}
        </Animated.View>
    );
};

type DynamicViewType = {
    position?: "absolute" | "relative";
    visible?: boolean;
    role?: ColorType;
    width: string | number;
    height: number;
    duration?: number;
    children?: JSX.Element | JSX.Element[] | string;
}
