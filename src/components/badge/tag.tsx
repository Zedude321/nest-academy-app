import React from 'react';
import { Box, Spacing, Text, Border, Queue } from '..';
import { CheckIcon, CloseLineIcon, DangerIcon, WarningIcon } from '../icons'
import { Avatar } from '../layout';

export const Tag: React.FC<TagType> = ({
  category = 'default',
  type = 'unremoveable',
  avatarUrl,
  children,
}) => {

  if (category === 'rounded') {
    return (
      <Box height={30} width={'auto'} alignSelf={'flex-start'}>
        <Border role={'primary300'} radius={1000} lineWidth={1} grow={1}>
          <Box flex={1} role={'primary100'}>
            <Spacing pv={avatarUrl ? 0.5 : 1} pl={avatarUrl ? 0.5 : 2} pr={2}>
              <Box flexDirection={'row'} justifyContent={'center'} alignItems={'center'}>
                {avatarUrl &&
                  <Spacing mr={1}>
                    <Avatar size={'XS'} url={avatarUrl} />
                  </Spacing>}
                <Text
                  width={'auto'}
                  fontFamily={'Montserrat'}
                  role={'primary500'}
                  type={'callout'}
                  textAlign={'center'}
                >
                  {children}
                </Text>
              </Box>
            </Spacing>
          </Box>
        </Border>
      </Box>
    )
  }

  if (category === 'message') {
    return (
      <Box height={30} width={'auto'} alignSelf={'flex-start'}>
        <Border role={type === 'warning' ? 'caution500' : type === 'danger' ? 'destructive500' : 'success500'} radius={4} lineWidth={1} grow={1}>
          <Box flex={1} role={type === 'warning' ? 'caution200' : type === 'danger' ? 'destructive200' : 'success200'}>
            <Spacing pv={1} ph={2}>
              <Box flexDirection={'row'} justifyContent={'center'} alignItems={'center'}>
                {type === 'warning' && <Spacing mr={1}><WarningIcon height={15} width={15} role={'caution500'} /></Spacing>}
                {type === 'danger' && <Spacing mr={1}><DangerIcon height={15} width={15} role={'destructive500'} /></Spacing>}
                {type === 'success' && <Spacing mr={1}><CheckIcon height={15} width={15} role={'success500'} /></Spacing>}
                <Text
                  width={'auto'}
                  fontFamily={'Montserrat'}
                  role={type === 'warning' ? 'caution500' : type === 'danger' ? 'destructive500' : 'success500'}
                  type={'callout'}
                  textAlign={'center'}
                >
                  {children}
                </Text>
              </Box>
            </Spacing>
          </Box>
        </Border>
      </Box>
    )
  }


  return (
    <Box height={30} width={'auto'} alignSelf={'flex-start'}>
      <Border role={'primary500'} radius={4} lineWidth={1} grow={1}>
        <Box flex={1} role={'primary100'}>
          <Spacing pv={1} ph={2}>
            <Box flexDirection={'row'} justifyContent={'center'} alignItems={'center'}>
              <Text
                width={'auto'}
                fontFamily={'Montserrat'}
                role={type === 'warning' ? 'caution500' : type === 'danger' ? 'destructive500' : type === 'success' ? 'success500' : 'primary500'}
                type={'callout'}
                textAlign={'center'}
              >
                {children}
              </Text>
              {type === 'removeable' && <Spacing ml={1}><CloseLineIcon height={12} width={12} role={'primary500'} /></Spacing>}
            </Box>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};

type TagType = {
  category: 'default' | 'message' | 'rounded';
  type: 'removeable' | 'unremoveable' | 'warning' | 'danger' | 'success' | 'user';
  avatarUrl?: string;
  children?: JSX.Element | JSX.Element[] | string;
};