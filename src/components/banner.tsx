import React from 'react' 
import { SafeAreaView, View, StyleSheet } from 'react-native'
import { colors, useTheme } from './theme-provider'
// import { Text } from '../components';
import styled from 'styled-components/native'
import { BannerCriticalIcon, BannerSuccessIcon, BannerWarningIcon, BannerInfoIcon } from './icons/bannerIcons'
import {Text} from '../components';
import { Box, Spacing, Stack } from './layout'

// const { caution400, caution300, caution200, success400, success300, success200, primary400, primary300, primary200, destructive400, destructive300, destructive200 } = colors

const mappingToShadows: any = {
    warning:  'caution400',
    info: 'primary500',
    critical: 'destructive400',
    success: 'success400'
}
const mappingToColors: any = {
    warning: 'caution200',
    info: 'primary100',
    critical: 'destructive200',
    success: 'success200',
}
const mappingToCircle: any = {
    warning: 'caution300',
    info: 'primary200',
    critical: 'destructive300',
    success: 'success300',
}

export const Banner = ({type='warning', moreInfo=true, header='Мэдэгдэл', desc='Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!'}) => {
    const { colors } = useTheme();
    const styles = StyleSheet.create({
        banner: {
            padding: 16,
            borderRadius: 4,
            backgroundColor: colors[mappingToColors[type]], 
            shadowColor: colors[mappingToShadows[type]],
            shadowOffset: {
                width: 0,
                height: -6,
            },
            shadowOpacity: 1,
            shadowRadius: 0,
            elevation: 5,
            flexDirection: 'row',
            marginTop: 20,
            width: '100%',
            display: 'flex',
            justifyContent: 'space-between'
        },
        bannerContainer: {
            // marginRight: 30,
            alignItems: 'center',
            justifyContent: 'center',
            // flexWrap: 'wrap',
            width: '100%',
            // width: windowW-40,
            shadowColor: colors[mappingToShadows[type]],
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.2,
            shadowRadius: 2,
            elevation: 5,
            borderRadius: 3,
            // padding: 7
        },
        header: {
            marginTop: 6,
            fontSize: 17,
            color: '#172B4D',
            // fontFamily: 'Montserrat',
            fontWeight: 'bold'
        },
        desc: {
            marginTop: 6,
            fontSize: 16,
            color: '#172B4D',
        },
        link: {
            marginTop: 18,
            fontSize: 16,
            color: '#172B4D',
        },
        iconContainer: {
            backgroundColor: colors[mappingToCircle[type]],
            width: 32,
            height: 32,
            borderRadius: 16,
            justifyContent: 'center',
            alignItems: 'center'
        }
    })

    console.log(mappingToColors[type]);

    return (
        <View style={styles.bannerContainer}>
            
                <View style={styles.banner}>
                            <View style={styles.iconContainer} >
                                {type == 'info' && <BannerInfoIcon></BannerInfoIcon>}
                                {type == 'warning' && <BannerWarningIcon></BannerWarningIcon>}
                                {type == 'critical' && <BannerCriticalIcon></BannerCriticalIcon>}
                                {type == 'success' && <BannerSuccessIcon></BannerSuccessIcon>}   
                            </View>
                            <Box flex={1}>
                                <Spacing ml={1} >
                                    <Stack size={1}>
                                        <Spacing mt={1} ><Text bold type={'headline'}>{header}</Text></Spacing>
                                        <Text numberOfLines={3} type={'callout'}>{desc}</Text>
                                        {moreInfo && <Spacing mt={3}><Text>Мэдээлэл авах</Text></Spacing>}
                                    </Stack>
                                </Spacing>
                            </Box>
                </View>
            
        </View>
    )
}



