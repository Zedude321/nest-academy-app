import React from 'react';
import { Box, Text, Button, LeftArrowIcon } from '../../components'
import { useNavigation } from '@react-navigation/native';

export const TopNavigation: React.FC<WarningType> = ({ type, name }) => {

    const navigation = useNavigation();

    const BackNavigation = () => {
        return (
            <Box flex={1} flexDirection='row' alignItems='center' justifyContent='center'>
                <Button onPress={() => {navigation.goBack()}} category='text'>
                    <Box height={64} width={50} justifyContent='center' alignItems='center'>
                        <LeftArrowIcon />
                    </Box>
                </Button>
                <Box flex={1} justifyContent='center' alignItems='center'>
                    <Text textAlign='center' fontFamily='Montserrat' type='title3' bold={true}>{name}</Text>
                </Box>
                <Box height={64} width={50}>
                </Box>
            </Box>
        )
    }

    return (
        <Box width='100%' height={64} justifyContent='center' alignItems='center' top={0}>
            {type === 'back' ? <BackNavigation /> : <Box />}
        </Box>
    )
};
type WarningType = {
    type: 'back',
    name?: string,
};