import * as React from "react"
import Svg, { Path } from "react-native-svg"

interface Props {
   width?: number | string;
   height?: number | string;
   color?: string;
}

export const PlayerIcon: React.FC<Props> = ({ width = 50, height = 49, color = "#172B4D", ...others }) => {
   return (
      <Svg width={width} height={height} viewBox="0 0 50 49" fill="none" {...others}>
         <Path
            d="M24.629 44.916c-11.244 0-20.358-9.14-20.358-20.416 0-11.277 9.114-20.417 20.358-20.417 11.243 0 20.357 9.14 20.357 20.417 0 11.276-9.114 20.416-20.357 20.416zM21.824 17.18a.812.812 0 00-1.267.678v13.283a.819.819 0 00.855.814.812.812 0 00.412-.136l9.932-6.64a.816.816 0 000-1.36l-9.934-6.639h.002z"
            fill={color}
         />
      </Svg>
   )
}