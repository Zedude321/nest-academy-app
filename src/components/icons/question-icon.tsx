import * as React from "react"
import Svg, { Path } from "react-native-svg"
import { useTheme } from "../theme-provider"
import { IconType } from "../types"

export const QuestionIcon: React.FC<IconType> = ({ width = 24, height = 25, role = 'black' }) => {
    const { colors } = useTheme();
    return (
        <Svg width={width} height={height} viewBox="0 0 24 25" fill="none">
            <Path
                d="M15 4.005H5v16h14v-12h-4v-4zM3 2.997c0-.548.447-.992.999-.992H16l5 5v13.993a1 1 0 01-.993 1.007H3.993A1 1 0 013 21.013V2.997zm8 12.008h2v2h-2v-2zm0-8h2v6h-2v-6z"
                fill={colors[role]}
            />
        </Svg>
    )
}