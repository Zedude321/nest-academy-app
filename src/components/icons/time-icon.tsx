import * as React from "react"
import Svg, { Path } from "react-native-svg"
import { useTheme } from "../theme-provider"
import { IconType } from "../types"

export const TimeIcon: React.FC<IconType> = ({ width = 24, height = 25, role = 'black' }) => {
    const { colors } = useTheme();

    return (
        <Svg width={width} height={height} viewBox="0 0 24 25" fill="none">
            <Path
                d="M12 2.005c5.52 0 10 4.48 10 10s-4.48 10-10 10-10-4.48-10-10 4.48-10 10-10zm0 18c4.42 0 8-3.58 8-8s-3.58-8-8-8-8 3.58-8 8 3.58 8 8 8zm3.536-12.95l1.414 1.414-4.95 4.95-1.414-1.414 4.95-4.95z"
                fill={colors[role]}
            />
        </Svg>
    )
}