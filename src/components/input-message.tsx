import React from 'react';
import { View } from 'react-native';
import { Border } from './core';
import { InputIcon } from './icons/input-icon';
import { Box, Spacing } from './layout';

type InputMessageType = {
    role: 'default' | 'warning' | 'error' | 'success';
    children?: JSX.Element | JSX.Element[] | string;
};

export const InputMessage: React.FC<InputMessageType> = ({ role, children }) => {
    const roleToColor = {
        'default': 'primary100',
        'warning': 'caution200',
        'error': 'destructive200',
        'success': 'success200'
    }

    const InfoIcon = () => {
        return (
            <View style={{ marginRight: 12 }}>
                <InputIcon role={roleToColor[role]} />
            </View>
        )
    }

    return (
        <Border radius={4}>
            <Box role={roleToColor[role]} width='100%' justifyContent='flex-start' alignItems='center' flexDirection='row'>
                <Spacing ph={8} pv={14}>
                    <>
                        <InfoIcon />
                        {children}
                    </>
                </Spacing>
            </Box>
        </Border>
    )
}