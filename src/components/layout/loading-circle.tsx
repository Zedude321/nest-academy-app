import React, { useEffect } from 'react';
import { View, StyleSheet, Animated } from 'react-native';
import { Easing } from 'react-native-reanimated';
import { LoadingCircleIcon } from '../icons';

export const LoadingCircle: React.FC<LoadingCircle> = ({ width, height }) => {
    const x = new Animated.Value(0);

    const spin = x.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })

    useEffect(() => {
        Animated.loop(
            Animated.timing(x, {
                toValue: 1,
                duration: 3000,
                easing: Easing.linear,
                useNativeDriver: true,
            })
        ).start();
    }, [])

    const styles = StyleSheet.create({
        loadingContainer: {
            width: width ? width :  48,
            height: height ? height : 47,
        },
    });

    return (
        <Animated.View style={[styles.loadingContainer, {
            transform: [{ rotate: spin }]
        }]}>
            <LoadingCircleIcon />
        </Animated.View>

    )
};

type LoadingCircle = {
    width?: number | string;
    height?: number | string;
};