import React from 'react';
import _ from 'lodash';
import { Box, Text, Shadow, Button, Spacing } from '../components'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { optionalCallExpression } from '@babel/types';
import { PlayIcon, LockIcon } from './icons';
import { Image, StyleSheet } from 'react-native';

type LessonType = {
    type?: 'locked' | 'unlocked';
    name?: string;
    url?: string | undefined;
    desc?: string;
    onPress: Function
};

export const LessonDashboard: React.FC<LessonType> = ({ type, name, url, desc, onPress }) => {
    return (
        <Shadow role={'gray'} radius={0} opacity={0.15} h={2} w={2}>
            <TouchableOpacity onPress={() => onPress()} style={styles.container}>
                <Box flex={1} flexDirection={'row'} alignItems={'center'}>
                    <Box flex={1.5} height={64} width={64}>
                        <Spacing ml={3}>
                            <Image style={styles.image} source={{ uri: url }} />
                        </Spacing>
                    </Box>
                    <Box flex={2.5} flexDirection={'column'}>
                        <Text type={'callout'} fontFamily={'Montserrat'} bold>{name}</Text>
                        <Text type={'caption1'} role={'primary400'}>{desc}</Text>
                    </Box>
                    <Box flex={1.15} justifyContent={'center'} alignItems={'center'}>
                        <Button category={'fill'} type={'primary'} width={40} height={40} onPress={() => onPress()}>
                            {
                                type === 'unlocked' && (
                                    <PlayIcon />
                                )
                            }
                            {
                                type === 'locked' && (
                                    <LockIcon />
                                )
                            }
                        </Button>
                    </Box>
                </Box>
            </TouchableOpacity>
        </Shadow>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        borderRadius: 8,
        width: 342,
        height: 92,
    },
    image: {
        width: 64,
        height: 64,
        borderRadius: 4
    }
})