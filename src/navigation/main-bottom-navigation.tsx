import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomTab } from './bottom-tab';
import { HomeScreen, TestScreen } from '../screen';
import { NavigationRoutes } from './navigation-params';
import { MainTopNav } from './top-nav';

const Tab = createBottomTabNavigator();

const MainBottomNavigation = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomTab {...props} />}>
            <Tab.Screen name={NavigationRoutes.Home} component={HomeScreen} />
            <Tab.Screen name={NavigationRoutes.Course} component={TestScreen} />
            <Tab.Screen name={NavigationRoutes.Profile} component={TestScreen} />
        </Tab.Navigator>
    );
}

export default MainBottomNavigation;