
import React, { createContext, useEffect, useState } from 'react';
import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'
export const AuthUserProvider = ({ children }) => {
    const [state, setState] = useState();
    const [user, setuser] = useState();
    const [confirmation, setconfirmation] = useState()
    const [number, setnumber] = useState()
    useEffect(() => {
        if (!auth) {
            return;
        }
        const subscribe = auth().onAuthStateChanged(async authUser => {
            if (authUser) {
                console.log(authUser.uid)
                const cred = firestore().collection('users').doc(authUser.uid).get()
                setState({ ready: true, user: cred })
            } else {
                setState({ ready: false, user: null })
            }
        });
        return () => subscribe()
    }, [auth]);
    const sendCode = (number: number) => {
        setnumber(number);
        auth().signInWithPhoneNumber(`+976${number}`)
            .then((confirmationResult: any) => {
                console.log('confirm set')
                setconfirmation(confirmationResult)
            }).catch((error: any) => {
                console.log(error)
            });
    };
    const verifyCode = (code: number, setUserState: Function, state: any) => {
        confirmation.confirm(code).then((result: any) => {
            let user = result.user;
            console.log(user)
            if(setUserState && state){
                setUserState({ ...state, uid: user.uid })
            }
        }).catch((error: any) => {
            console.log(error)
        });
    };
    const signOut = () => {
        auth()
            .signOut()
            .then(() => console.log('User signed out!'));
    };
    return (
        <AuthContext.Provider value={{
            state,
            user,
            confirmation,
            verifyCode,
            sendCode,
            signOut,
        }}>
            {children}
        </AuthContext.Provider>
    );
};
export const AuthContext = createContext(null)