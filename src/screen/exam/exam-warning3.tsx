import React from 'react';
import { Box, Warning, CancelIllustration, TopNavigation, Text } from '../../components';
import { useNavigation } from '@react-navigation/native';

export const ExamWarningScreen3 = () => {
    const navigation = useNavigation();
    return (
        <Box flex={1} alignItems='center' justifyContent='space-evenly' role='white'>
            <TopNavigation name='Шалгалт' type='back' />
            <CancelIllustration />
            <Warning onPress={() => { navigation.navigate('ExamWarning4') }} title='Санамж #3' buttonname='Дараах'>
                <Text role='primary500' type='body' textAlign='center' width={275}>Элсэлтийн онлайн шалгалтыг нэг хүн нэг л удаа өгөх боломжтой.</Text>
            </Warning>
        </Box>
    );
};
