import React from 'react';
import { Box, Warning, LaunchIllustration, TopNavigation, Text, TimeIcon, QuestionIcon } from '../../components';
import { useNavigation } from '@react-navigation/native';

export const ExamWarningScreen4 = () => {
    const navigation = useNavigation();
    return (
        <Box flex={1} alignItems='center' justifyContent='space-evenly' role='white'>
            <TopNavigation name='Шалгалт' type='back' />
            <LaunchIllustration />
            <Warning onPress={() => { navigation.navigate('Home') }} title='Шалгалтаа эхлэх' titleAlign='left' buttonname='Эхлэх'>
                <Box width='100%' height={60} justifyContent='center' alignItems='center'>
                    <Box height={60} width='80%' flexDirection='row' justifyContent='space-between'>
                        <Box height={60} justifyContent='space-between' width={100}>
                            <Text type='subheading' role='black'>Хугацаа</Text>
                            <Box flexDirection='row' height={20} justifyContent='center'>
                                <Box width={30}>
                                    <TimeIcon />
                                </Box>
                                <Text type='body' role='primary500' bold={true}>30 минут</Text>
                            </Box>
                        </Box>
                        <Box flex={1} justifyContent='center' alignItems='center'>
                            <Box width={1} height='100%' role='gray' />
                        </Box>
                        <Box height={60} justifyContent='space-between' width={100}>
                            <Text type='subheading' role='black'>Асуулт</Text>
                            <Box flexDirection='row' height={20} justifyContent='center'>
                                <Box width={30}>
                                    <QuestionIcon />
                                </Box>
                                <Text type='body' role='primary500' bold={true}>30 минут</Text>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Warning>
        </Box>
    );
};
