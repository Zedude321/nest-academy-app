import * as React from "react"
import Svg, { Path } from "react-native-svg"

interface Props {
   width?: number | string;
   height?: number | string;
   color?: string;
}

export const EllipseIcon: React.FC<Props> = ({ width = 79, height = 81, color = "#303030", ...others }) => {
   return (
      <Svg width={width} height={height} viewBox="0 0 79 81" fill="none" {...others}>
         <Path
            opacity={0.5}
            d="M70.125 60.325c-.122 28.635-23.771 52.027-53.043 52.027s-52.721-23.392-52.6-52.027c.123-28.635 23.772-52.027 53.043-52.027 29.272 0 52.722 23.392 52.6 52.027z"
            stroke="#00DCF0"
            strokeWidth={16}
         />
      </Svg>
   )
}