import React from 'react';
import { FlatList, SafeAreaView } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Border, Box, Queue, Spacing, Text } from '../../components';
import { NestLogo } from '../../components/icons/nest-logo';
import { NewsCard, Register, TryCourse } from './';
import { BackgroundImage } from '../../components/layout/background-image';
const exampleImg = require('../../assets/images/image1.png');
const hop = require('../../assets/images/hop.png');
const leap = require('../../assets/images/leap.png');

const Title: React.FC<any> = ({ title1, title2, title1Role = 'black', title2Role = 'black' }) => {
    return (
        <Queue size={1}>
            <Text type={'headline'} role={title1Role} bold width={'auto'}>{title1}</Text>
            <Text type={'headline'} role={title2Role} bold width={'auto'}>{title2}</Text>
        </Queue>
    )
}

const Course = ({ source }: any) => {
    return (
        <Border radius={8}>
            <TouchableOpacity>
                <BackgroundImage source={source} height={167} width={167}>
                </BackgroundImage>
            </TouchableOpacity>
        </Border>
    )
}

const cardsInfo = [
    {
        id: 'card-0',
        title: 'Demo Day 2020 арга хэмжээ амжилттай болж өнгөрлөө.',
        date: '2021 / 4 / 13',
        source: exampleImg
    },
    {
        id: 'card-1',
        title: 'Demo Day 2020 арга хэмжээ амжилттай болж өнгөрлөө.',
        date: '2021 / 4 / 13',
        source: exampleImg
    },
    {
        id: 'card-2',
        title: 'Demo Day 2020 арга хэмжээ амжилттай болж өнгөрлөө.',
        date: '2021 / 4 / 13',
        source: exampleImg
    }
]

const cources = [hop, leap]

export const HomeScreen: React.FC<any> = () => {
    return (
        <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }}>
            <Spacing ph={4}>
                <Box flexDirection={'row'} alignItems={'center'} height={42}>
                    <NestLogo />
                </Box>
            </Spacing>
            <ScrollView>
                <Register />
                <Spacing ph={4}>
                    <Box height={'auto'}>
                        <Spacing mv={4}>
                            <Title title1={'ҮНДСЭН'} title2={'ХӨТӨЛБӨР'} />
                        </Spacing>
                        <FlatList
                            data={cources}
                            renderItem={({ item }) => <Course source={item} />}
                            keyExtractor={(_, index) => index + 'course'}
                            ItemSeparatorComponent={() => <Box width={16} />}
                            showsHorizontalScrollIndicator={false}
                            horizontal
                        />
                    </Box>
                </Spacing>
                <Spacing pl={4} mv={2}>
                    <Box height={'auto'}>
                        <Spacing mb={4}>
                            <Title title1={'NEST'} title2={'ACADEMY'} title2Role={'accentNest'} />
                        </Spacing>
                        <FlatList
                            data={cardsInfo}
                            renderItem={({ item }) => <NewsCard source={item.source} title={item.title} date={item.date} />}
                            keyExtractor={(item) => item.id}
                            ItemSeparatorComponent={() => <Box width={16} />}
                            showsHorizontalScrollIndicator={false}
                            horizontal
                        />
                    </Box>
                </Spacing>
                <Spacing pl={4} mv={2}>
                    <Box height={'auto'}>
                        <Spacing mb={4}>
                            <Title title1={'NEST'} title2={'FOUNDATION'} title2Role={'caution400'} />
                        </Spacing>
                        <FlatList
                            data={cardsInfo}
                            renderItem={({ item }) => <NewsCard source={item.source} title={item.title} date={item.date} />}
                            keyExtractor={(item) => item.id}
                            ItemSeparatorComponent={() => <Box width={16} />}
                            showsHorizontalScrollIndicator={false}
                            horizontal
                        />
                    </Box>
                </Spacing>
                <Spacing pl={4} mv={2}>
                    <Box height={'auto'}>
                        <Spacing mb={4}>
                            <Title title1={'NEST'} title2={'SOLUTIONS'} title2Role={'primary500'} />
                        </Spacing>
                        <FlatList
                            data={cardsInfo}
                            renderItem={({ item }) => <NewsCard source={item.source} title={item.title} date={item.date} />}
                            keyExtractor={(item) => item.id}
                            ItemSeparatorComponent={() => <Box width={16} />}
                            showsHorizontalScrollIndicator={false}
                            horizontal
                        />
                    </Box>
                </Spacing>
                <Spacing ph={4} mv={2} mb={10}>
                    <TryCourse />
                </Spacing>
            </ScrollView>
        </SafeAreaView >
    );
};
