import * as React from "react"
import Svg, { G, Mask, Path, Rect } from "react-native-svg"

interface Props {
   width?: number | string;
   height?: number | string;
   color?: string;
}

export const RectangleIcon: React.FC<Props> = ({ width = 114, height = 46, color = "#303030", ...others }) => {
   return (
      <Svg width={114} height={46} viewBox="0 0 114 46" fill="none" {...others}>
         <G opacity={0.5}>
            <Mask id="a" fill="#fff">
               <Rect
                  width={113.053}
                  height={111.578}
                  rx={9}
                  transform="matrix(1 0 -.00426 1 .571 .487)"
               />
            </Mask>
            <Rect
               width={113.053}
               height={111.578}
               rx={9}
               transform="matrix(1 0 -.00426 1 .571 .487)"
               stroke="#007AFF"
               strokeWidth={32}
               mask="url(#a)"
            />
         </G>
      </Svg>
   )
}