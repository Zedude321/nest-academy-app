import * as React from "react"
import Svg, { G, Mask, Path, Rect } from "react-native-svg"

interface Props {
   width?: number | string;
   height?: number | string;
   color?: string;
}

export const TriangleIcon: React.FC<Props> = ({ width = 138, height = 104, color = "#303030", ...others }) => {
   return (
      <Svg width={width} height={height} viewBox="0 0 138 104" fill="none" {...others}>
         <Path
            opacity={0.5}
            d="M10.79 102.477L69.106 10.84l57.533 91.637H10.791zm-2.928 4.603l.002-.004-.002.004z"
            stroke="#FF9500"
            strokeWidth={16}
         />
      </Svg>
   )
}