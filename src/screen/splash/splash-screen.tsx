import React from 'react';
import { SafeAreaView } from 'react-native';
import Swiper from 'react-native-swiper';

import { Box } from '../../components';
import { SplashFirst } from './splash-first';
import { SplashSecond } from './splash-second';
import { SplashThird } from './splash-third';

export const SplashScreen = () => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFF' }}>
            <Box flex={1}>
                <Swiper showsPagination={false} loop={false}>
                    <SplashFirst />
                    <SplashSecond />
                    <SplashThird />
                </Swiper>
            </Box>
        </SafeAreaView>
    );
};