import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { Button } from '../../components/core';
import { Stack, Queue, Box, Text, Spacing } from '../../components';

export const TestScreen = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView style={{ flex: 1 }}>
        <Box flex={1}>
          <Spacing mh={5}>
            <Stack size={4} role={'caution200'}>
              <Text type={'largeTitle'} bold>Typography</Text>
              <Text type={'title1'}>Нэг түмэн инженер</Text>
              <Text type={'title2'}>Нэг түмэн инженер</Text>
              <Text type={'title3'}>Нэг түмэн инженер</Text>
              <Text type={'headline'}>Нэг түмэн инженер</Text>
              <Text type={'body'}>Нэг түмэн инженер</Text>
              <Text type={'callout'}>Нэг түмэн инженер</Text>
              <Queue size={3} alignItems={'center'} justifyContent={'flex-end'}>
                <Text type={'subheading'}>Нэг түмэн инженер</Text>
                <Text type={'caption1'}>Нэг түмэн инженер</Text>
                <Text type={'caption2'}>Нэг түмэн инженер</Text>
              </Queue>
              <Text numberOfLines={4} type={'body'}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                eros dui, pellentesque eget condimentum sed, interdum non lectus.
                Proin elementum risus elit, eget tempor lorem viverra vitae
            </Text>
              <Text width={'50%'} numberOfLines={2} type={'body'}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                eros dui, pellentesque eget condimentum sed, interdum non lectus.
                Proin elementum risus elit, eget tempor lorem viverra vitae
            </Text>
            </Stack>
          </Spacing>
          <Box alignItems="center">
            <Stack width={'90%'} size={4}>
              <Button onPress={() => console.log('handled')} size="s">
                <Text
                  fontFamily={'Montserrat'}
                  role={'white'}
                  type={'callout'}
                  textAlign={'center'}
                  bold>
                  Товчлуур
                </Text>
              </Button>
              <Button
                onPress={() => console.log('handled')}
                status="disabled">
                <Text
                  fontFamily={'Montserrat'}
                  role={'white'}
                  type={'callout'}
                  textAlign={'center'}
                  bold>
                  Товчлуур
                </Text>
              </Button>
              <Button
                onPress={() => console.log('handled')}
                status="active"
                type="destructive">
                <Text
                  fontFamily={'Montserrat'}
                  role={'white'}
                  type={'callout'}
                  textAlign={'center'}
                  bold>
                  Товчлуур
                </Text>
              </Button>
              <Button
                onPress={() => console.log('handled')}
                category="ghost"
                type="destructive">
                <Text
                  fontFamily={'Montserrat'}
                  role={'destructive500'}
                  type={'callout'}
                  textAlign={'center'}
                  bold>
                  Товчлуур
                </Text>
              </Button>
              <Button
                onPress={() => console.log('handled')}
                width={90}
                category="text">
                <Text
                  fontFamily={'Montserrat'}
                  role={'primary500'}
                  type={'callout'}
                  textAlign={'center'}
                  bold>
                  Товчлуур
                </Text>
              </Button>
              <Button
                onPress={() => console.log('handled')}
                category="text"
                type="destructive"
                status="active"
                width={90}>
                <Text
                  fontFamily={'Montserrat'}
                  role={'destructive500'}
                  type={'callout'}
                  textAlign={'center'}
                  bold>
                  Товчлуур
                </Text>
              </Button>
            </Stack>
          </Box>
          <Box flex={1} flexDirection={'row'}>
            <Box flex={1} role={'primary100'}></Box>
            <Box width={150} role={'primary200'}></Box>
            <Box flex={2} role={'primary300'}></Box>
          </Box>
        </Box>
      </ScrollView>
    </SafeAreaView>
  );
};
